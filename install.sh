sudo apt install  xmonad libghc-xmonad-contrib-dev xmobar kitty nitrogen picom dmenu ttf-ubuntu-font-family fish;
mkdir ~/.xmonad;
touch ~/.xmobarrc;
git clone https://github.com/Arielnoder/xmonad.git;
git clone https://github.com/Arielnoder/xmobar.git;
git clone https://gitlab.com/Arielnoder/kitty.git;
cd xmonad-script-ubuntu;
cp install.sh $HOME;
cp $HOME/xmobar/xmobarrc .xmobarrc;
cp $HOME/xmonad/xmonad.hs $HOME/.xmonad/ 
cp $HOME/kitty/kitty.conf $HOME/.config/kitty/kitty.conf

